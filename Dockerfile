FROM centos:7

RUN yum update -y
RUN yum install -y gcc gcc-c++ glibc-devel make rsyslog git time

ENV GOLANG_VERSION 1.5.1
ENV GOLANG_DOWNLOAD_URL https://golang.org/dl/go$GOLANG_VERSION.linux-amd64.tar.gz
ENV GOLANG_DOWNLOAD_SHA1 46eecd290d8803887dec718c691cc243f2175fe0

RUN curl -fsSL "$GOLANG_DOWNLOAD_URL" -o golang.tar.gz \
	&& echo "$GOLANG_DOWNLOAD_SHA1  golang.tar.gz" | sha1sum -c - \
	&& tar -C /usr/local -xzf golang.tar.gz \
	&& rm golang.tar.gz

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH

RUN go get github.com/revel/revel
RUN go get github.com/revel/cmd/revel
RUN mkdir src/github.com/hild1985
WORKDIR $GOPATH/src/github.com/hild1985/

RUN git clone https://github.com/hild1985/binary.git

CMD revel run github.com/hild1985/binary

EXPOSE 9000